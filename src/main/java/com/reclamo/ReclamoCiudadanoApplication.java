package com.reclamo;


import com.reclamo.controllers.ReclamoController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;


@SpringBootApplication
public class ReclamoCiudadanoApplication {
	
	
	public static void main(String[] args) throws IOException {
		new File(ReclamoController.directory).mkdirs();
		SpringApplication.run(ReclamoCiudadanoApplication.class, args);


	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}
}

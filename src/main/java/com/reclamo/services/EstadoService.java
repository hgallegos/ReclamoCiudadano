package com.reclamo.services;

import com.reclamo.domain.Estado;

import java.util.List;

public interface EstadoService {

	Estado saveEstado(Estado estado);

	Estado getEstadoById(Integer id);

	List<Estado> listAllEstados();

	void deleteEstado(Integer id);

}

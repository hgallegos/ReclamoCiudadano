package com.reclamo.services;

import com.reclamo.domain.Seguir;

import java.util.List;

public interface SeguirService {

	Seguir saveSeguir(Seguir seguir);

	Seguir getSeguirById(Integer id);

	List<Seguir> listAllSeguir();

	void deleteSeguir(Integer id);

	void deleteByQuery(String mail, Integer id_reclamo);

}

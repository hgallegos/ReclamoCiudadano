package com.reclamo.services;

import com.reclamo.domain.Imagen;
import com.reclamo.repositories.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImagenServiceImpl implements ImagenService {

	@Autowired
	ImagenRepository repository;
	
	@Override
	public Imagen saveImagen(Imagen imagen) {
		// TODO Auto-generated method stub
		return repository.save(imagen);
	}

	@Override
	public Imagen getImagenById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public List<Imagen> listAllImagens() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void deleteImagen(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

}

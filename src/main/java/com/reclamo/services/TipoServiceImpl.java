package com.reclamo.services;

import java.util.List;

import com.reclamo.domain.Tipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reclamo.repositories.TipoRepository;

@Service
public class TipoServiceImpl implements TipoService {

	private TipoRepository tipoRepository;
	
	@Autowired
	public void setTipoRepository(TipoRepository tipoRepository) {
		this.tipoRepository = tipoRepository;
	}
	
	@Override
	public List<Tipo> listAllTipos() {
		// TODO Auto-generated method stub
		return tipoRepository.findAll();
	}

	@Override
	public Tipo getTipoById(Integer id) {
		// TODO Auto-generated method stub
		return tipoRepository.findOne(id);
	}

	@Override
	public Tipo saveTipo(Tipo tipo) {
		// TODO Auto-generated method stub
		return tipoRepository.save(tipo);
	}

	@Override
	public void deleteTipo(Integer id) {
		// TODO Auto-generated method stub
		tipoRepository.delete(id);
	}


}

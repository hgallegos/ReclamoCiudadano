package com.reclamo.services;

import com.reclamo.domain.Estado;
import com.reclamo.repositories.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoServiceImpl implements EstadoService {

	@Autowired
	EstadoRepository repository;
	
	@Override
	public Estado saveEstado(Estado estado) {
		// TODO Auto-generated method stub
		return repository.save(estado);
	}

	@Override
	public Estado getEstadoById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public List<Estado> listAllEstados() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void deleteEstado(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

}

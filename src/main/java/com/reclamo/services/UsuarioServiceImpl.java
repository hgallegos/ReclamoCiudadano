package com.reclamo.services;

import java.util.List;

import com.reclamo.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reclamo.repositories.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository repository;
	
	@Override
	public Usuario saveUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		return repository.save(usuario);
	}

	@Override
	public Usuario getUsuarioById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public Usuario Login(String correo, String password) {
		// TODO Auto-generated method stub
		//return repository.findOne(id);
		//System.out.println(correo);
		List<Usuario> lista = listAllUsuarios();
		for(int i = 0; i < lista.size(); i++){
			if(lista.get(i).getMail().equals(correo) && lista.get(i).getPassword().equals(password)){
				return lista.get(i);
			}
		}
		//System.out.println(lg.getIdUsuario());
		return null;
	}

	public Usuario obtieneDatosPorCorreo(String correo){
		List<Usuario> lista = listAllUsuarios();
		for(int i = 0; i < lista.size(); i++){
			if(lista.get(i).getMail().equals(correo)){
				return lista.get(i);
			}
		}
		return  null;
	}

	@Override
	public boolean verificaSiExiste(String correo) {
		// TODO Auto-generated method stub
		List<Usuario> lista = listAllUsuarios();
		for(int i = 0; i < lista.size(); i++){
			if(lista.get(i).getMail().equals(correo)){
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Usuario> listAllUsuarios() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void deleteUsuario(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	@Override
	public List<Usuario> search(String query) {
		return repository.findResultsbyName(query);
	}

}

package com.reclamo.services;

import com.reclamo.domain.Tipo;

import java.util.List;

public interface TipoService {

	List<Tipo> listAllTipos();
	
	Tipo getTipoById(Integer id);
	
	Tipo saveTipo(Tipo tipo);
	
	void deleteTipo(Integer id);

	
}

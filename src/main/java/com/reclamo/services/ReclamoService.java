package com.reclamo.services;

import com.reclamo.domain.Reclamo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReclamoService {

	Reclamo saveReclamo(Reclamo reclamo);

	Reclamo getReclamoById(Integer id);

	List<Reclamo> listAllReclamos();

	void deleteReclamo(Integer id);

    List<Reclamo> search(String search);


	Page<Reclamo> findAll(Pageable pageable);

	List<Object[]> frecuenciaRelativaTipo(String fecha1, String fecha2);

	List<Object[]> frecuenciaRelativaGravedad(String fecha1, String fecha2);

	List<Object[]> frecuenciaRelativaEstado(String fecha1, String fecha2);

	List<Object[]> tablaResumen(String fecha1, String fecha2);

	Long totalReclamoEstadistica(String fecha1, String fecha2);

	List<Reclamo> queryMapa(String tipo, String gravedad, String estado, String fecha1, String fecha2);
}

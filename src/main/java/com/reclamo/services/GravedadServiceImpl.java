package com.reclamo.services;

import java.util.List;

import com.reclamo.domain.Gravedad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reclamo.repositories.GravedadRepository;

@Service
public class GravedadServiceImpl implements GravedadService {

	@Autowired
	private GravedadRepository repository;

	@Override
	public List<Gravedad> listAllGravedades() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Gravedad getGravedadById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public Gravedad saveGravedad(Gravedad gravedad) {
		// TODO Auto-generated method stub
		return repository.save(gravedad);
	}

	@Override
	public void deleteGravedad(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	

	
	

}

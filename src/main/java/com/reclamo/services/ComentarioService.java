package com.reclamo.services;

import com.reclamo.domain.Comentario;

import java.util.List;

public interface ComentarioService {

	Comentario saveComentario(Comentario comentario);

	void guardaComentario(String comentario, Integer id_usuario, Integer id_reclamo);

	Comentario getComentarioById(Integer id);

	List<Comentario> listAllComentarios();

	List<Comentario> listComentariosXReclamo(Integer id_reclamo);

	void deleteComentario(Integer id);

}

package com.reclamo.services;

import java.util.List;

import com.reclamo.domain.Reclamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.reclamo.repositories.ReclamoRepository;

@Service
public class ReclamoServiceImpl implements ReclamoService {

	@Autowired
	ReclamoRepository repository;
	
	@Override
	public Reclamo saveReclamo(Reclamo reclamo) {
		// TODO Auto-generated method stub
		return repository.save(reclamo);
	}

	@Override
	public Reclamo getReclamoById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public List<Reclamo> listAllReclamos() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void deleteReclamo(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	@Override
	public List<Reclamo> search(String search) {
		if(repository.findResultsByName(search).isEmpty()) {
			return repository.findResultsByUbicacion(search);
		}else
			return repository.findResultsByName(search);
	}

	@Override
	public Page<Reclamo> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public List<Object[]> frecuenciaRelativaTipo(String fecha1, String fecha2) {
		// TODO Auto-generated method stub
		return repository.frecuenciaRelativaTipo(fecha1, fecha2);
	}

	@Override
	public List<Object[]> frecuenciaRelativaGravedad(String fecha1, String fecha2) {
		// TODO Auto-generated method stub
		return repository.frecuenciaRelativaGravedad(fecha1, fecha2);
	}

	@Override
	public List<Object[]> frecuenciaRelativaEstado(String fecha1, String fecha2) {
		// TODO Auto-generated method stub
		return repository.frecuenciaRelativaEstado(fecha1, fecha2);
	}

	@Override
	public List<Object[]> tablaResumen(String fecha1, String fecha2) {
		// TODO Auto-generated method stub
		return repository.tablaResumen(fecha1, fecha2);
	}


	@Override
	public Long totalReclamoEstadistica(String fecha1, String fecha2) {
		// TODO Auto-generated method stub
		return repository.totalReclamosEstadistica(fecha1, fecha2);
	}

	@Override
	public List<Reclamo> queryMapa(String tipo, String gravedad, String estado, String fecha1, String fecha2){
		// TODO Auto-generated method stub
		return repository.queryMapa(tipo, gravedad, estado, fecha1, fecha2);
	}


}

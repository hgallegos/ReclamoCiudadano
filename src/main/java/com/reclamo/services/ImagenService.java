package com.reclamo.services;

import com.reclamo.domain.Imagen;

import java.util.List;

public interface ImagenService {

	Imagen saveImagen(Imagen imagen);

	Imagen getImagenById(Integer id);

	List<Imagen> listAllImagens();

	void deleteImagen(Integer id);

}

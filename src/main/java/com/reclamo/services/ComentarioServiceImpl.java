package com.reclamo.services;

import com.reclamo.domain.Comentario;
import com.reclamo.repositories.ComentarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ComentarioServiceImpl implements ComentarioService {

	@Autowired
	ComentarioRepository repository;
	
	@Override
	public Comentario saveComentario(Comentario comentario) {
		// TODO Auto-generated method stub
		return repository.save(comentario);
	}

	@Override
	public Comentario getComentarioById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public List<Comentario> listAllComentarios() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public List<Comentario> listComentariosXReclamo(Integer id_reclamo) {
		// TODO Auto-generated method stub
		return repository.buscaComentariosXReclamo(id_reclamo);
	}

	@Override
	public void deleteComentario(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	@Override
	@Transactional
	public void guardaComentario(String comentario, Integer id_usuario, Integer id_reclamo){
		repository.creaUserOP(comentario,id_usuario,id_reclamo);
	}

}

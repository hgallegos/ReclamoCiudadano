package com.reclamo.services;

import com.reclamo.domain.Usuario;

import java.util.List;

public interface UsuarioService {

	Usuario saveUsuario(Usuario usuario);

	Usuario getUsuarioById(Integer id);

	List<Usuario> listAllUsuarios();

	void deleteUsuario(Integer id);

	List<Usuario> search(String query);

	Usuario Login(String correo, String password);

	boolean verificaSiExiste(String correo);

	Usuario obtieneDatosPorCorreo(String correo);

}

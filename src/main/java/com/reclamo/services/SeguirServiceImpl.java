package com.reclamo.services;

import com.reclamo.domain.Seguir;
import com.reclamo.repositories.SeguirRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class SeguirServiceImpl implements SeguirService {

	@Autowired
	SeguirRepository repository;
	
	@Override
	public Seguir saveSeguir(Seguir seguir) {
		// TODO Auto-generated method stub
		return repository.save(seguir);
	}

	@Override
	public Seguir getSeguirById(Integer id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	@Override
	public List<Seguir> listAllSeguir() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public void deleteSeguir(Integer id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	@Override
	@Transactional
	public void deleteByQuery(String mail, Integer id_reclamo) {
		repository.deleteByQuery(mail,id_reclamo);
	}

}

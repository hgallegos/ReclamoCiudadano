package com.reclamo.services;

import com.reclamo.domain.Gravedad;

import java.util.List;

public interface GravedadService {

	List<Gravedad> listAllGravedades();
	
	Gravedad getGravedadById(Integer id);
	
	Gravedad saveGravedad(Gravedad gravedad);
	
	void deleteGravedad(Integer id);
	
}

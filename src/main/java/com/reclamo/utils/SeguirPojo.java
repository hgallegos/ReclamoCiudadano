package com.reclamo.utils;

/**
 * Created by hans6 on 12-12-2016.
 */
public class SeguirPojo {

    private int idReclamo;
    private String mail;

    public int getIdReclamo() {
        return idReclamo;
    }

    public void setIdReclamo(int idReclamo) {
        this.idReclamo = idReclamo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}

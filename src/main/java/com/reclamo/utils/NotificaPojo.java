package com.reclamo.utils;

/**
 * Created by hans6 on 13-12-2016.
 */
public class NotificaPojo {

    private String mail;

    private int contador;

    private int idReclamo;

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getIdReclamo() {
        return idReclamo;
    }

    public void setIdReclamo(int idReclamo) {
        this.idReclamo = idReclamo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}

package com.reclamo.controllers;

import com.reclamo.domain.Tipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.reclamo.services.TipoService;

@Controller
public class TipoController {
	
	
	private TipoService tipoService;
	
	@Autowired
	public void setTipoService(TipoService tipoService) {
		this.tipoService = tipoService;
	}
	
	@RequestMapping("tipo/nuevo")
	public String nuevoTipo(Model model){
		model.addAttribute(new Tipo());
		model.addAttribute("titulo", "Agregar Tipo");
		return "tipo/formtipo";
	}
	
	@RequestMapping(value = "tipo", method= RequestMethod.POST)
	public String guardarTipo(Tipo tipo){
		tipoService.saveTipo(tipo);
		return "redirect:/tipo/" + tipo.getIdTipo();
	}
	
	@RequestMapping("tipo/{id}")
	public String verTipo(@PathVariable Integer id, Model model){
		model.addAttribute("tipo", tipoService.getTipoById(id));
		return "tipo/vertipo";
	}
	


    @RequestMapping(value = "/tipos", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("tipos", tipoService.listAllTipos());
        return "tipo/tipos";
    }
    
    @RequestMapping("tipo/modificar/{id}")
    public String modificar(@PathVariable Integer id, Model model){
    	model.addAttribute("tipo", tipoService.getTipoById(id));
    	model.addAttribute("titulo", "Modificar Tipo");
    	return "tipo/formtipo";
    }

    @RequestMapping("tipo/eliminar/{id}")
    public String eliminar(@PathVariable Integer id, Model model){
    	tipoService.deleteTipo(id);
    	return "redirect:/tipos";
    }
	
	
}

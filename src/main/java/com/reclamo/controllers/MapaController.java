package com.reclamo.controllers;

import com.reclamo.domain.Reclamo;
import com.reclamo.services.EstadoService;
import com.reclamo.services.GravedadService;
import com.reclamo.services.ReclamoService;
import com.reclamo.services.TipoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MapaController {
    @Autowired
    private ReclamoService reclamoService;

    @Autowired
    private TipoService tipoService;

    @Autowired
    private EstadoService estadoService;

    @Autowired
    private GravedadService gravedadService;

    @RequestMapping("mapa/index")
    public String index(Model model){
        model.addAttribute("listaTipos", tipoService.listAllTipos());
        model.addAttribute("listaEstados", estadoService.listAllEstados());
        model.addAttribute("listaGravedades", gravedadService.listAllGravedades());

        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Reclamo> aux = reclamoService.listAllReclamos();
            long resultadosQuery = aux.size();
            String json = mapper.writeValueAsString(reclamoService.listAllReclamos());
            model.addAttribute("prueba", json);
            model.addAttribute("totalQuery", resultadosQuery);
        }catch (Exception e){
            System.out.println("Excepción de JACKSON: JSON PROCESSING EXEPCTION");
        }
        model.addAttribute("titulo", "Mapa General de Reclamos");
        return "mapa/vistageneral";

    }

    @RequestMapping(value= "mapa/filtro", method = RequestMethod.GET)
    public String filtro(@RequestParam String tipo,
                         @RequestParam String gravedad,
                         @RequestParam String estado,
                         @RequestParam String fecha1, @RequestParam  String fecha2, Model model){
        model.addAttribute("listaTipos", tipoService.listAllTipos());
        model.addAttribute("listaEstados", estadoService.listAllEstados());
        model.addAttribute("listaGravedades", gravedadService.listAllGravedades());
        if(tipo.equals("all")){
            tipo = "%";
        }

        if(gravedad.equals("all")){
            gravedad = "%";
        }

        if(estado.equals("all")){
            estado = "%";
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Reclamo> aux = reclamoService.queryMapa(tipo, gravedad, estado, fecha1, fecha2);
            long resultadosQuery = aux.size();
            String json = mapper.writeValueAsString(reclamoService.queryMapa(tipo, gravedad, estado, fecha1, fecha2));
            model.addAttribute("prueba", json);
            model.addAttribute("totalQuery", resultadosQuery);
        }catch (Exception e){
            System.out.println("Excepción de JACKSON: JSON PROCESSING EXEPCTION");
        }
        model.addAttribute("titulo", "Mapa General de Reclamos");
        return "mapa/vistageneral";

    }
}

package com.reclamo.controllers;

import com.reclamo.domain.*;
import com.reclamo.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
public class ReclamoController {
	public static final String directory = System.getProperty("user.dir") + "/src/main/resources/static/img/reclamos/";
	
	@Autowired
	private ReclamoService reclamoService;
	
	@Autowired
	private TipoService tipoService;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private GravedadService gravedadService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private ImagenService imagenService;

	@Autowired
	private ComentarioService comentarioService;

	@RequestMapping("reclamo/nuevo")
	public String nuevoreclamo(Model model){
		model.addAttribute(new Reclamo());
		model.addAttribute("titulo", "Agregar Reclamo");
		model.addAttribute("listaTipos", tipoService.listAllTipos());
		model.addAttribute("listaEstados", estadoService.listAllEstados());
		model.addAttribute("listaGravedades", gravedadService.listAllGravedades());
		File foto = new File(directory);
		model.addAttribute("fotos", foto.listFiles());
		return "reclamo/formreclamo";
	}
	
	@RequestMapping(value = "reclamo", method= RequestMethod.POST)
	public String guardarreclamo(@RequestParam("fotos") MultipartFile[] fotos, Reclamo reclamo, HttpServletRequest request) throws IOException{
		HttpSession session = request.getSession();

		Usuario usuario = usuarioService.getUsuarioById((Integer.parseInt(""+session.getAttribute("idUser"))));
		reclamo.setUsuario(usuario);
		reclamoService.saveReclamo(reclamo);

		Logger log = LoggerFactory.getLogger(this.getClass());
		log.info(""+fotos.length);

			for(MultipartFile foto: fotos){

				if(!foto.isEmpty()){
					File file = new File(directory + foto.getOriginalFilename());
					Imagen imagen = new Imagen();
					imagen.setArchivo(foto.getOriginalFilename());
					imagen.setReclamo(reclamo);
					imagenService.saveImagen(imagen);
					foto.transferTo(file);
				}



			}


		return "redirect:/reclamo2/" + reclamo.getIdReclamo();
	}


	@RequestMapping("reclamo/{id}")
	public String verreclamo(@PathVariable Integer id, Model model){
		model.addAttribute("reclamo", reclamoService.getReclamoById(id));
		return "reclamo/verreclamo";
	}

	@RequestMapping("reclamo2/{id}")
	public String verreclamo2(@PathVariable Integer id, Model model, HttpServletRequest request){
		HttpSession session = request.getSession();
		String correo = (String) session.getAttribute("usuario");
		Reclamo reclamo = reclamoService.getReclamoById(id);
		model.addAttribute("reclamo", reclamo);
		String claseCss = "btn btn-primary";
		List<Seguir> listaSeguir = null;
		if(correo != null){
			listaSeguir = usuarioService.obtieneDatosPorCorreo(correo).getSeguirs();
		}
		if(listaSeguir != null){
			for(Seguir sigue: listaSeguir){
				if(sigue.getReclamo().getIdReclamo() == id){
					claseCss = "btn btn-danger";
					System.out.println(sigue.getIdSeguir());
					break;
				}
			}
		}

		//clase css para definir un si el usuario actual sigue un reclamo
		model.addAttribute("claseCss", claseCss);
		model.addAttribute("reclamo", reclamoService.getReclamoById(id));
		model.addAttribute("comentarios", comentarioService.listComentariosXReclamo(id));
		return "reclamo/verreclamo2";
	}

	@RequestMapping(value = "reclamo2/comentar", method= RequestMethod.POST)
	public String creaComentario(@RequestParam("comment") String comentario, @RequestParam("id_reclamo") Integer id_reclamo, @RequestParam("id_usuario") Integer id_usuario){
		comentarioService.guardaComentario(comentario,id_usuario,id_reclamo);
		return "redirect:/reclamo2/" + id_reclamo;
	}

	@RequestMapping(value = "reclamo2/delete", method = RequestMethod.GET)
	public String deleteaComentario(@RequestParam("comentario") Integer comentario, @RequestParam("reclamo") Integer reclamo){
		comentarioService.deleteComentario(comentario);
		System.out.println("Elimina el " + comentario);
		return "redirect:/reclamo2/" + reclamo;
	}

    @RequestMapping(value = "/reclamos", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("reclamos", reclamoService.listAllReclamos());
		Logger log = LoggerFactory.getLogger(this.getClass());
		log.info(directory);
        return "reclamo/reclamos";
    }

	@RequestMapping(value = "/reclamos2", method = RequestMethod.GET)
	public String list2(Model model, @PageableDefault(value=6) Pageable pageable){
		model.addAttribute("reclamos", reclamoService.findAll(pageable));
		return "reclamo/reclamos2";
	}

    @RequestMapping("reclamo/modificar/{id}")
    public String modificar(@PathVariable Integer id, Model model){
    	model.addAttribute("reclamo", reclamoService.getReclamoById(id));
    	model.addAttribute("titulo", "Modificar Reclamo");
    	model.addAttribute("listaTipos", tipoService.listAllTipos());
		model.addAttribute("listaEstados", estadoService.listAllEstados());
		model.addAttribute("listaGravedades", gravedadService.listAllGravedades());
		File foto = new File(directory);
		model.addAttribute("fotos", foto.listFiles());
    	return "reclamo/formreclamo";
    }

    @RequestMapping("reclamo/eliminar/{id}")
    public String eliminar(@PathVariable Integer id, Model model){
    	Reclamo reclamo = reclamoService.getReclamoById(id);
		Estado estado = estadoService.getEstadoById(3);
    	reclamo.setEstado(estado);
    	reclamoService.saveReclamo(reclamo);
    	return "redirect:/reclamos";
    }

	@RequestMapping(value = "/searchReclamo", method=RequestMethod.GET)
	public String busqueda(@RequestParam("s") String search, Model model) {
		model.addAttribute("titulo", "Resultados de la Busqueda");
		model.addAttribute("reclamos", reclamoService.search(search));

		return "reclamo/reclamos2";
	}
	
	
}

package com.reclamo.controllers;

import com.reclamo.domain.Estado;
import com.reclamo.domain.Reclamo;
import com.reclamo.domain.Seguir;
import com.reclamo.domain.Usuario;
import com.reclamo.services.EstadoService;
import com.reclamo.services.ReclamoService;
import com.reclamo.services.SeguirService;
import com.reclamo.services.UsuarioService;
import com.reclamo.utils.NotificaPojo;
import com.reclamo.utils.SeguirPojo;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by hans6 on 12-12-2016.
 */
@RestController
public class SeguirRestController {

    private SimpleMailMessage simpleMailMessage;

    @Autowired
    private SeguirService seguirService;

    @Autowired
    private ReclamoService reclamoService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private EstadoService estadoService;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private MailSender mailSender;

    @RequestMapping(value= "/seguir", method = RequestMethod.POST, produces = "application/json")
    public String seguir(@RequestBody SeguirPojo seguir, HttpServletRequest request){
        System.out.println(seguir.getMail());
        Usuario usuario = usuarioService.obtieneDatosPorCorreo(seguir.getMail());
        Reclamo reclamo = reclamoService.getReclamoById(seguir.getIdReclamo());
        Seguir seguirSave = new Seguir();
        seguirSave.setReclamo(reclamo);
        seguirSave.setUsuario(usuario);
        seguirSave.setMail(seguir.getMail());
        seguirService.saveSeguir(seguirSave);

        System.out.println("Enviando..."+request.getLocalAddr());

        this.simpleMailMessage = new SimpleMailMessage();
        this.simpleMailMessage.setSubject("Sigues un reclamo");
        this.simpleMailMessage.setFrom("reclamociudadanoici@gmail.com");
        this.simpleMailMessage.setTo(seguir.getMail());

        SimpleMailMessage msg = new SimpleMailMessage(this.simpleMailMessage);
        msg.setText("Sigues el reclamo "+reclamo.getNombre());

        try{
            this.mailSender.send(msg);
        }
        catch(MailException ex){
            System.err.println(ex.getMessage());
        }
        System.out.println("Enviado");

        return "{\"success\":1}";
    }

    @RequestMapping(value = "/dejarSeguir", method = RequestMethod.POST, produces = "application/json")
    public String dejarSeguir(@RequestBody SeguirPojo seguirPojo){
        Usuario usuario = usuarioService.obtieneDatosPorCorreo(seguirPojo.getMail());
        Reclamo reclamo = reclamoService.getReclamoById(seguirPojo.getIdReclamo());
        Seguir seguir = new Seguir();
        seguir.setReclamo(reclamo);
        seguir.setUsuario(usuario);
        seguir.setMail(seguirPojo.getMail());
        seguirService.deleteByQuery(seguirPojo.getMail(),reclamo.getIdReclamo());

        return "{\"success\":2}";
    }

    @RequestMapping(value = "/solucionado", method = RequestMethod.POST, produces = "application/json")
    public String solucionado(@RequestBody NotificaPojo notificaPojo, HttpServletRequest request){
        int contador = notificaPojo.getContador()+1;
        Reclamo reclamo = reclamoService.getReclamoById(notificaPojo.getIdReclamo());
        reclamo.setContador(contador);
        if(contador == 6 ){
            Estado estado = estadoService.getEstadoById(2);
            reclamo.setEstado(estado);

            System.out.println("Enviando..."+request.getLocalAddr());

            this.simpleMailMessage = new SimpleMailMessage();
            this.simpleMailMessage.setSubject("Sigues un reclamo");
            this.simpleMailMessage.setFrom("reclamociudadanoici@gmail.com");
            this.simpleMailMessage.setTo(notificaPojo.getMail());

            SimpleMailMessage msg = new SimpleMailMessage(this.simpleMailMessage);
            msg.setText("El reclamo "+reclamo.getNombre()+" cambio a estado solucionado");

            try{
                this.mailSender.send(msg);
            }
            catch(MailException ex){
                System.err.println(ex.getMessage());
            }
            System.out.println("Enviado");
        }
        reclamoService.saveReclamo(reclamo);

        return "{\"success\":"+ contador + "}";
    }

    @RequestMapping(value = "/noSolucionado", method = RequestMethod.POST, produces = "application/json")
    public String noSolucionado(@RequestBody NotificaPojo notificaPojo, HttpServletRequest request){
        int contador = notificaPojo.getContador()-1;
        Reclamo reclamo = reclamoService.getReclamoById(notificaPojo.getIdReclamo());
        reclamo.setContador(contador);
        if(contador == 5 ){
            Estado estado = estadoService.getEstadoById(1);
            reclamo.setEstado(estado);

            System.out.println("Enviando..."+request.getLocalAddr());

            this.simpleMailMessage = new SimpleMailMessage();
            this.simpleMailMessage.setSubject("Sigues un reclamo");
            this.simpleMailMessage.setFrom("reclamociudadanoici@gmail.com");
            this.simpleMailMessage.setTo(notificaPojo.getMail());

            SimpleMailMessage msg = new SimpleMailMessage(this.simpleMailMessage);
            msg.setText("El reclamo "+reclamo.getNombre()+" cambio a estado no solucionado");

            try{
                this.mailSender.send(msg);
            }
            catch(MailException ex){
                System.err.println(ex.getMessage());
            }
            System.out.println("Enviado");
        }
        reclamoService.saveReclamo(reclamo);

        return "{\"success\":"+ contador + "}";
    }
}

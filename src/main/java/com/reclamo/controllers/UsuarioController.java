package com.reclamo.controllers;

import com.reclamo.domain.Usuario;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.reclamo.services.UsuarioService;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	@RequestMapping("registrar")
	public String registrarUsuario(Model model){
		model.addAttribute(new Usuario());
		model.addAttribute("titulo", "Crear Cuenta");
		return "usuario/creaCuenta";
	}

	@RequestMapping("usuario/nuevo")
	public String nuevoUsuario(Model model){
		model.addAttribute(new Usuario());
		model.addAttribute("titulo", "Agregar Usuario");
		return "usuario/formusuario";
	}

	@RequestMapping(value = "usuario", method= RequestMethod.POST)
	public String guardarUsuario(Usuario usuario){
		usuarioService.saveUsuario(usuario);
		return "redirect:/usuario/" + usuario.getIdUsuario();
	}

	@RequestMapping(value = "usuarioRegistra", method= RequestMethod.POST)
	public String guardarUsuarioRegistra(Usuario usuario,HttpServletRequest request, Model model){
		if(usuarioService.verificaSiExiste(usuario.getMail())){
			model.addAttribute("titulo", "Uups... Error");
			return "usuario/creaCuenta_Error";
		}
		usuarioService.saveUsuario(usuario);
		HttpSession session = request.getSession();
		session.setAttribute("usuario", usuario.getMail());
		session.setAttribute("status", "login");
		return "redirect:/";
	}
	
	@RequestMapping("usuario/{id}")
	public String verUsuario(@PathVariable Integer id, Model model){
		model.addAttribute("usuario", usuarioService.getUsuarioById(id));
		return "usuario/verusuario";
	}
	


    @RequestMapping(value = "/usuarios", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("usuarios", usuarioService.listAllUsuarios());
        model.addAttribute("link", "'/usuario/eliminar/' + usuario.idUsuario}");
        return "usuario/usuarios";
    }
    
    @RequestMapping("usuario/modificar/{id}")
    public String modificar(@PathVariable Integer id, Model model){
    	model.addAttribute("usuario", usuarioService.getUsuarioById(id));
    	model.addAttribute("titulo", "Modificar Usuario");
    	return "usuario/formusuario";
    }

    @RequestMapping("usuario/eliminar/{id}")
    public String eliminar(@PathVariable Integer id, Model model){
    	usuarioService.deleteUsuario(id);
    	return "redirect:/usuarios";
    }

	@RequestMapping(value = "/searchUsuario", method=RequestMethod.GET)
	public String busqueda(@RequestParam("s") String search, Model model) {
		model.addAttribute("titulo", "Resultados de la Busqueda");
		model.addAttribute("usuarios", usuarioService.search(search));

		return "usuario/usuarios";
	}
	
	
}

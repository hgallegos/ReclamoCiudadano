package com.reclamo.controllers;

import com.reclamo.domain.Gravedad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.reclamo.services.GravedadService;


@Controller
public class GravedadController {

	@Autowired
	private GravedadService gravedadService;

	
	@RequestMapping("gravedad/nuevo")
	public String nuevoTipo(Model model){
		model.addAttribute(new Gravedad());
		model.addAttribute("titulo", "Agregar Gravedad");
		return "gravedad/formgravedad";
	}
	
	@RequestMapping(value = "gravedad", method= RequestMethod.POST)
	public String guardarTipo(Gravedad gravedad){
		gravedadService.saveGravedad(gravedad);
		return "redirect:/gravedad/" + gravedad.getIdGravedad();
	}
	
	@RequestMapping("gravedad/{id}")
	public String verTipo(@PathVariable Integer id, Model model){
		model.addAttribute("gravedad", gravedadService.getGravedadById(id));
		return "gravedad/vergravedad";
	}
	


    @RequestMapping(value = "/gravedades", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("gravedades", gravedadService.listAllGravedades());
        return "gravedad/gravedades";
    }
    
    @RequestMapping("gravedad/modificar/{id}")
    public String modificar(@PathVariable Integer id, Model model){
    	model.addAttribute("gravedad", gravedadService.getGravedadById(id));
    	model.addAttribute("titulo", "Modificar Gravedad");
    	return "gravedad/formgravedad";
    }

    @RequestMapping("gravedad/eliminar/{id}")
    public String eliminar(@PathVariable Integer id, Model model){
    	gravedadService.deleteGravedad(id);
    	return "redirect:/gravedades";
    }
}

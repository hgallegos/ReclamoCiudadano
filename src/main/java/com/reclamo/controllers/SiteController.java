package com.reclamo.controllers;

import com.reclamo.domain.Usuario;
import com.reclamo.services.UsuarioService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;

import javax.servlet.http.HttpServlet;

@Controller
public class SiteController {

	@Autowired
	private UsuarioService usuarioService;

	@RequestMapping("/")
	public String index(HttpServletRequest request, Model model){
		HttpSession session = request.getSession();
		if(session.getAttribute("usuario") != null){
			System.out.println(session.getAttribute("usuario"));
			Usuario dt = usuarioService.obtieneDatosPorCorreo((String) session.getAttribute("usuario"));
			model.addAttribute("bienvenido", "Bienvenido, " + dt.getNombre());
		}else{
			model.addAttribute("bienvenido", "Explora");
		}
		return "index";
	}

	@RequestMapping(value = "/login", method= RequestMethod.POST)
	public String loginUsuario(@RequestParam("correo") String correo, @RequestParam("password") String password, HttpServletRequest request){
		Usuario usr = usuarioService.Login(correo,password);
		if(usr != null){
			HttpSession session = request.getSession();
			session.setAttribute("usuario", correo);
			if(usr.getIdPerfil() == 2) {
				session.setAttribute("status", "login");
			}else{
				session.setAttribute("status", "admin");
			}
			session.setAttribute("idUser", usr.getIdUsuario());
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/logout", method= RequestMethod.POST)
	public String logout(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.invalidate();
		return "redirect:/";
	}
}

package com.reclamo.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reclamo.domain.Reclamo;
import com.reclamo.services.EstadoService;
import com.reclamo.services.GravedadService;
import com.reclamo.services.ReclamoService;
import com.reclamo.services.TipoService;
import org.jxls.template.SimpleExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.reclamo.domain.Reporteaux;

/**
 * Created by Luis on 12-12-2016.
 */
@Controller
public class EstadisticaController {
    @Autowired
    private ReclamoService reclamoService;

    @Autowired
    private TipoService tipoService;

    @Autowired
    private EstadoService estadoService;

    @Autowired
    private GravedadService gravedadService;

    @RequestMapping("estadistica/index")
    public String list(Model model){
        //model.addAttribute("reclamos", reclamoService.listAllReclamos());
        //model.addAttribute("frecuenciaTipos", reclamoService.frecuenciaRelativaTipo());
        //model.addAttribute("frecuenciaGravedades", reclamoService.frecuenciaRelativaGravedad());
        //model.addAttribute("frecuenciaEstados", reclamoService.frecuenciaRelativaEstado());
        //model.addAttribute("tablaResumen", reclamoService.tablaResumen());

        return "estadistica/vistageneral";
    }

    @RequestMapping(value= "estadistica/forminicial", method = RequestMethod.GET)
    public String forminicial(Model model){
        return "estadistica/estadisticaFecha";
    }

    @RequestMapping(value= "estadistica/porfecha", method = RequestMethod.GET)
    public String porfecha(@RequestParam  String fecha1, @RequestParam  String fecha2, Model model){
        model.addAttribute("frecuenciaTipos", reclamoService.frecuenciaRelativaTipo(fecha1, fecha2));
        model.addAttribute("frecuenciaGravedades", reclamoService.frecuenciaRelativaGravedad(fecha1, fecha2));
        model.addAttribute("frecuenciaEstados", reclamoService.frecuenciaRelativaEstado(fecha1, fecha2));
        model.addAttribute("tablaResumen", reclamoService.tablaResumen(fecha1, fecha2));
        model.addAttribute("totalReclamosPeriodo", reclamoService.totalReclamoEstadistica(fecha1, fecha2));
        model.addAttribute("fecha1", fecha1);
        model.addAttribute("fecha2", fecha2);

        model.addAttribute("listaTipos", tipoService.listAllTipos());
        model.addAttribute("listaEstados", estadoService.listAllEstados());
        model.addAttribute("listaGravedades", gravedadService.listAllGravedades());


        return "estadistica/vistageneral";
    }


    @RequestMapping(value= "estadistica/detalle", method = RequestMethod.GET)
    public String detalle(@RequestParam  String fecha1,
                              @RequestParam  String tipo,
                              @RequestParam  String gravedad,
                              @RequestParam  String estado,
                              @RequestParam  String fecha2,
                              Model model){
        if(tipo.equals("all")){
            tipo = "%";
        }

        if(gravedad.equals("all")){
            gravedad = "%";
        }

        if(estado.equals("all")){
            estado = "%";
        }

        model.addAttribute("reclamos", reclamoService.queryMapa(tipo, gravedad, estado, fecha1, fecha2));

        return "estadistica/vistageneral2";
    }

    @RequestMapping(value = "estadistica/export", method = RequestMethod.GET)
    public void export(HttpServletResponse response, @RequestParam  String fecha1, @RequestParam  String fecha2) {
        //String fecha1 = "2016-01-01";
        //String fecha2 = "2017-12-31";

        List<Reporteaux> lista = new ArrayList<Reporteaux>();
        List<Object[]> fTipos = reclamoService.frecuenciaRelativaTipo(fecha1, fecha2);

        for(int i=0; i<fTipos.size(); i++){
            Reporteaux aux = new Reporteaux();
            aux.setId(fTipos.get(i)[0].toString());
            aux.setCategoria(fTipos.get(i)[1].toString());
            aux.setCantidad(fTipos.get(i)[2].toString());
            aux.setPorcentaje(fTipos.get(i)[3].toString());
            lista.add(aux);
        }

        List<String> headers = Arrays.asList("id", "categoria", "cantidad", "porcentaje");
        try {
            response.addHeader("Content-disposition", "attachment; filename=reporte-"+fecha1+"-"+fecha2+".xls");
            response.setContentType("application/vnd.ms-excel");
            new SimpleExporter().gridExport(headers, lista, "id, categoria, cantidad, porcentaje", response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            //log.warn(e.getMessage(), e);
        }
    }
}

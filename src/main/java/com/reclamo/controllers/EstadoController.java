package com.reclamo.controllers;

import com.reclamo.domain.Estado;
import com.reclamo.services.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EstadoController {
	
	@Autowired
	private EstadoService estadoService;
	
	
	@RequestMapping("estado/nuevo")
	public String nuevoEstado(Model model){
		model.addAttribute(new Estado());
		model.addAttribute("titulo", "Agregar Estado");
		return "estado/formestado";
	}
	
	@RequestMapping(value = "estado", method= RequestMethod.POST)
	public String guardarEstado(Estado estado){
		estadoService.saveEstado(estado);
		return "redirect:/estado/" + estado.getIdEstado();
	}
	
	@RequestMapping("estado/{id}")
	public String verEstado(@PathVariable Integer id, Model model){
		model.addAttribute("estado", estadoService.getEstadoById(id));
		return "estado/verestado";
	}
	


    @RequestMapping(value = "/estados", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("estados", estadoService.listAllEstados());
        model.addAttribute("link", "'/estado/eliminar/' + estado.idEstado}");
        return "estado/estados";
    }
    
    @RequestMapping("estado/modificar/{id}")
    public String modificar(@PathVariable Integer id, Model model){
    	model.addAttribute("estado", estadoService.getEstadoById(id));
    	model.addAttribute("titulo", "Modificar Estado");
    	return "estado/formestado";
    }

    @RequestMapping("estado/eliminar/{id}")
    public String eliminar(@PathVariable Integer id, Model model){
    	estadoService.deleteEstado(id);
    	return "redirect:/estados";
    }
	
	
}

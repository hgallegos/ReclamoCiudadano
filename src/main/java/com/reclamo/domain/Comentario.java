package com.reclamo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the comentario database table.
 * 
 */
@Entity
@NamedQuery(name="Comentario.findAll", query="SELECT c FROM Comentario c")
public class Comentario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_comentario")
	private int idComentario;

	private String comentario;

	//private String apellidos = "";

	//private String nombre = "";

	//bi-directional many-to-one association to Reclamo
	@ManyToOne
	@JsonBackReference
	private Reclamo reclamo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	private Usuario usuario;

	public Comentario() {
	}

	public int getIdComentario() {
		return this.idComentario;
	}

	public void setIdComentario(int idComentario) {
		this.idComentario = idComentario;
	}

	public String getComentario() {
		return this.comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Reclamo getReclamo() {
		return this.reclamo;
	}

	public void setReclamo(Reclamo reclamo) {
		this.reclamo = reclamo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	//public String getApellidos() { return this.apellidos; }

	//public void setApellidos(String apellidos) { this.apellidos = apellidos; }

	//public String getNombre() { return this.nombre; }

	//public void setNombre(String nombre) { this.nombre = nombre; }

}
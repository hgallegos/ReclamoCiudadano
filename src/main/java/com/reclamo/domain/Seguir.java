package com.reclamo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the seguir database table.
 * 
 */
@Entity
@NamedQuery(name="Seguir.findAll", query="SELECT s FROM Seguir s")
public class Seguir implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_seguir")
	private int idSeguir;

	private String mail;

	//bi-directional many-to-one association to Reclamo
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "Reclamo_id_reclamo", referencedColumnName = "id_reclamo")
	private Reclamo reclamo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "Usuario_id_usuario", referencedColumnName = "id_usuario")
	private Usuario usuario;

	public Seguir() {
	}

	public int getIdSeguir() {
		return this.idSeguir;
	}

	public void setIdSeguir(int idSeguir) {
		this.idSeguir = idSeguir;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Reclamo getReclamo() {
		return this.reclamo;
	}

	public void setReclamo(Reclamo reclamo) {
		this.reclamo = reclamo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
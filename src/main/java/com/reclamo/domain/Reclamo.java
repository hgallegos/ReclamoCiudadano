package com.reclamo.domain;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the reclamo database table.
 * 
 */
@Entity
@NamedQuery(name="Reclamo.findAll", query="SELECT r FROM Reclamo r")
public class Reclamo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_reclamo")
	private int idReclamo;

	private String descripcion;

	private String direccion;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date fecha;

	private double latitud;

	private double longitud;

	private String nombre;

	//bi-directional many-to-one association to Comentario
	@OneToMany(mappedBy="reclamo")
	@JsonIgnore
	private List<Comentario> comentarios;

	//bi-directional many-to-one association to Imagen
	@OneToMany(mappedBy="reclamo")
	@JsonManagedReference
	private List<Imagen> imagens;

	//bi-directional many-to-one association to Puntuacion
	@OneToMany(mappedBy="reclamo")
	@JsonIgnore
	private List<Puntuacion> puntuacions;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JsonManagedReference
	private Estado estado;

	//bi-directional many-to-one association to Gravedad
	@ManyToOne
	@JsonManagedReference
	private Gravedad gravedad;

	//bi-directional many-to-one association to Tipo
	@ManyToOne
	@JsonManagedReference
	private Tipo tipo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JsonIgnore
	private Usuario usuario;

	//bi-directional many-to-one association to Reporte
	@OneToMany(mappedBy="reclamo")
	@JsonIgnore
	private List<Reporte> reportes;

	//bi-directional many-to-one association to Seguir
	@OneToMany(mappedBy="reclamo")
	@JsonIgnore
	private List<Seguir> seguirs;

	private Integer contador;

	public Reclamo() {
	}

	public int getIdReclamo() {
		return this.idReclamo;
	}

	public void setIdReclamo(int idReclamo) {
		this.idReclamo = idReclamo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getLatitud() {
		return this.latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return this.longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Comentario> getComentarios() {
		return this.comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public Comentario addComentario(Comentario comentario) {
		getComentarios().add(comentario);
		comentario.setReclamo(this);

		return comentario;
	}

	public Comentario removeComentario(Comentario comentario) {
		getComentarios().remove(comentario);
		comentario.setReclamo(null);

		return comentario;
	}

	public List<Imagen> getImagens() {
		return this.imagens;
	}

	public void setImagens(List<Imagen> imagens) {
		this.imagens = imagens;
	}

	public Imagen addImagen(Imagen imagen) {
		getImagens().add(imagen);
		imagen.setReclamo(this);

		return imagen;
	}

	public Imagen removeImagen(Imagen imagen) {
		getImagens().remove(imagen);
		imagen.setReclamo(null);

		return imagen;
	}

	public List<Puntuacion> getPuntuacions() {
		return this.puntuacions;
	}

	public void setPuntuacions(List<Puntuacion> puntuacions) {
		this.puntuacions = puntuacions;
	}

	public Puntuacion addPuntuacion(Puntuacion puntuacion) {
		getPuntuacions().add(puntuacion);
		puntuacion.setReclamo(this);

		return puntuacion;
	}

	public Puntuacion removePuntuacion(Puntuacion puntuacion) {
		getPuntuacions().remove(puntuacion);
		puntuacion.setReclamo(null);

		return puntuacion;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Gravedad getGravedad() {
		return this.gravedad;
	}

	public void setGravedad(Gravedad gravedad) {
		this.gravedad = gravedad;
	}

	public Tipo getTipo() {
		return this.tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Reporte> getReportes() {
		return this.reportes;
	}

	public void setReportes(List<Reporte> reportes) {
		this.reportes = reportes;
	}

	public Reporte addReporte(Reporte reporte) {
		getReportes().add(reporte);
		reporte.setReclamo(this);

		return reporte;
	}

	public Reporte removeReporte(Reporte reporte) {
		getReportes().remove(reporte);
		reporte.setReclamo(null);

		return reporte;
	}

	public List<Seguir> getSeguirs() {
		return this.seguirs;
	}

	public void setSeguirs(List<Seguir> seguirs) {
		this.seguirs = seguirs;
	}

	public Seguir addSeguir(Seguir seguir) {
		getSeguirs().add(seguir);
		seguir.setReclamo(this);

		return seguir;
	}

	public Seguir removeSeguir(Seguir seguir) {
		getSeguirs().remove(seguir);
		seguir.setReclamo(null);

		return seguir;
	}

	@Basic
	@Column(name = "contador")
	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Reclamo reclamo = (Reclamo) o;

		if (contador != null ? !contador.equals(reclamo.contador) : reclamo.contador != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return contador != null ? contador.hashCode() : 0;
	}
}
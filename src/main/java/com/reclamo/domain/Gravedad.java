package com.reclamo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the gravedad database table.
 * 
 */
@Entity
@NamedQuery(name="Gravedad.findAll", query="SELECT g FROM Gravedad g")
public class Gravedad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_gravedad")
	private int idGravedad;

	private String descripcion;

	private String nombre;

	//bi-directional many-to-one association to Reclamo
	@OneToMany(mappedBy="gravedad")
	@JsonBackReference
	private List<Reclamo> reclamos;

	public Gravedad() {
	}

	public int getIdGravedad() {
		return this.idGravedad;
	}

	public void setIdGravedad(int idGravedad) {
		this.idGravedad = idGravedad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Reclamo> getReclamos() {
		return this.reclamos;
	}

	public void setReclamos(List<Reclamo> reclamos) {
		this.reclamos = reclamos;
	}

	public Reclamo addReclamo(Reclamo reclamo) {
		getReclamos().add(reclamo);
		reclamo.setGravedad(this);

		return reclamo;
	}

	public Reclamo removeReclamo(Reclamo reclamo) {
		getReclamos().remove(reclamo);
		reclamo.setGravedad(null);

		return reclamo;
	}

}
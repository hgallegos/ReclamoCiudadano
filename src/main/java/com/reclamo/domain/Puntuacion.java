package com.reclamo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the puntuacion database table.
 * 
 */
@Entity
@NamedQuery(name="Puntuacion.findAll", query="SELECT p FROM Puntuacion p")
public class Puntuacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_puntuacion")
	private int idPuntuacion;

	private int valoracion;

	//bi-directional many-to-one association to Reclamo
	@ManyToOne
	@JsonBackReference
	private Reclamo reclamo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	private Usuario usuario;

	public Puntuacion() {
	}

	public int getIdPuntuacion() {
		return this.idPuntuacion;
	}

	public void setIdPuntuacion(int idPuntuacion) {
		this.idPuntuacion = idPuntuacion;
	}

	public int getValoracion() {
		return this.valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}

	public Reclamo getReclamo() {
		return this.reclamo;
	}

	public void setReclamo(Reclamo reclamo) {
		this.reclamo = reclamo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
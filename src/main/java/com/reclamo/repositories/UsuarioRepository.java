package com.reclamo.repositories;

import com.reclamo.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(value = "SELECT * FROM usuario where upper(nombre) like %?1%" , nativeQuery = true)
    List<Usuario> findResultsbyName(String query);

    @Query(value = "SELECT * FROM usuario WHERE mail = ?1 AND password = ?2", nativeQuery = true)
    List<Usuario> loginUsuario(String email, String password);
}

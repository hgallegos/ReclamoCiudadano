package com.reclamo.repositories;

import com.reclamo.domain.Gravedad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GravedadRepository extends JpaRepository<Gravedad, Integer> {

}

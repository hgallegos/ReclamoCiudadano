package com.reclamo.repositories;

import com.reclamo.domain.Reclamo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReclamoRepository extends JpaRepository<Reclamo, Integer> {

    @Query(value = "SELECT * FROM reclamo where upper(nombre) like %?1%" , nativeQuery = true)
    List<Reclamo> findResultsByName(String name);

    @Query(value = "SELECT * FROM reclamo where upper(direccion) like %?1%" , nativeQuery = true)
    List<Reclamo> findResultsByUbicacion(String ubicacion);

    @Query(value = "SELECT tipo.id_tipo AS ID, tipo.nombre AS Nombre, count(*) as Cantidad, (count(*)/(SELECT count(*) FROM reclamo WHERE reclamo.fecha BETWEEN ?1 and ?2)*100) AS Porcentaje" +
            " FROM reclamo JOIN tipo ON reclamo.Tipo_id_tipo = tipo.id_tipo" +
            " WHERE reclamo.fecha BETWEEN ?1 and ?2" +
            " GROUP BY Tipo_id_tipo" , nativeQuery = true)
    List<Object[]> frecuenciaRelativaTipo(String fecha1, String fecha2);

    @Query(value = "SELECT gravedad.id_gravedad AS ID, gravedad.nombre AS Nombre, count(*) as Cantidad, (count(*)/(SELECT count(*) FROM reclamo WHERE reclamo.fecha BETWEEN ?1 and ?2)*100) AS Porcentaje" +
            " FROM reclamo JOIN gravedad ON Gravedad_id_gravedad = gravedad.id_gravedad" +
            " WHERE reclamo.fecha BETWEEN ?1 and ?2" +
            " GROUP BY Gravedad_id_gravedad" , nativeQuery = true)
    List<Object[]> frecuenciaRelativaGravedad(String fecha1, String fecha2);

    @Query(value = "SELECT estado.id_estado AS ID, estado.descripcion AS Nombre, count(*) as Cantidad, (count(*)/(SELECT count(*) FROM reclamo WHERE reclamo.fecha BETWEEN ?1 and ?2)*100) AS Porcentaje" +
            " FROM reclamo JOIN estado ON Estado_id_estado = estado.id_estado" +
            " WHERE reclamo.fecha BETWEEN ?1 and ?2" +
            " GROUP BY Estado_id_estado" , nativeQuery = true)
    List<Object[]> frecuenciaRelativaEstado(String fecha1, String fecha2);

    @Query(value = "SELECT tipo.nombre AS Tipo, gravedad.nombre AS Gravedad, estado.descripcion AS Estado, count(*) as Reclamos, (count(*)/(SELECT count(*) FROM reclamo WHERE reclamo.fecha BETWEEN ?1 and ?2)*100) AS Porcentaje" +
            " FROM reclamo JOIN tipo ON reclamo.Tipo_id_tipo = tipo.id_tipo JOIN gravedad ON Gravedad_id_gravedad = gravedad.id_gravedad JOIN estado ON Estado_id_estado = estado.id_estado" +
            " WHERE reclamo.fecha BETWEEN ?1 and ?2" +
            " GROUP BY Tipo_id_tipo, Gravedad_id_gravedad, Estado_id_estado" , nativeQuery = true)
    List<Object[]> tablaResumen(String fecha1, String fecha2);

    @Query(value = "SELECT count(*) as Cantidad" +
            " FROM reclamo" +
            " WHERE reclamo.fecha BETWEEN ?1 and ?2" , nativeQuery = true)
    Long totalReclamosEstadistica(String fecha1, String fecha2);

    @Query(value = "SELECT * FROM reclamo WHERE Tipo_id_tipo LIKE ?1 AND Gravedad_id_gravedad LIKE ?2 AND Estado_id_estado LIKE ?3 AND fecha BETWEEN ?4 AND ?5" , nativeQuery = true)
    List<Reclamo> queryMapa(String tipo, String gravedad, String estado, String fecha1, String fecha2);

}

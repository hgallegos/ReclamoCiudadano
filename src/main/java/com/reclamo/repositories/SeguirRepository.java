package com.reclamo.repositories;

import com.reclamo.domain.Seguir;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SeguirRepository extends JpaRepository<Seguir, Integer> {

    @Modifying
    @Query(value = "DELETE FROM seguir where mail = ?1 and Reclamo_id_reclamo = ?2 " , nativeQuery = true)
    void deleteByQuery(String mail, Integer idReclamo);

}

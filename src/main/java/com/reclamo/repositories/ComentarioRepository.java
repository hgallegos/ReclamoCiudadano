package com.reclamo.repositories;

import com.reclamo.domain.Comentario;
import com.reclamo.domain.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComentarioRepository extends JpaRepository<Comentario, Integer> {

    //@Query(value = "SELECT A.id_comentario AS id, A.comentario AS cm, B.nombre AS nmbr, B.apellidos AS apll FROM comentario AS A JOIN usuario AS B ON (A.usuario_id_usuario = B.id_usuario) WHERE A.reclamo_id_reclamo = ?1" , nativeQuery = true)
    //List<Comentario> buscaComentariosXReclamo(Integer id_reclamo);

    @Query(value = "SELECT * FROM comentario JOIN usuario ON (usuario.id_usuario = comentario.usuario_id_usuario) WHERE comentario.reclamo_id_reclamo = ?1" , nativeQuery = true)
    List<Comentario> buscaComentariosXReclamo(Integer id_reclamo);

    @Modifying
    @Query(value = "INSERT INTO comentario (comentario, reclamo_id_reclamo, usuario_id_usuario) VALUES (?1, ?3, ?2)" , nativeQuery = true)
    void creaUserOP(String comentario, Integer id_usuario, Integer id_reclamo);



}
